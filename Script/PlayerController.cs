﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    // Start is called before the first frame update
    private Rigidbody2D rig2;
    private float speed = 1.0f;
    private float jamp = 3.5f;
    private bool isStage;
    private bool isLad;
    public Text gText;

    void Start()
    {
        rig2 = GetComponent<Rigidbody2D>();
        isStage = false;
        isLad = false;
        gText = GetComponent<Text>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (Input.GetKey(KeyCode.D))
        {
            transform.position += transform.right * speed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.A))
        {
            transform.position -= transform.right * speed * Time.deltaTime;
        }

        if (isStage == true)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                GetComponent<Rigidbody2D>().velocity = new Vector2(0, jamp);
                isStage = false;
            }
        }
        if (isLad == true)
        {
            if (Input.GetKey(KeyCode.W))
            {
                GetComponent<Rigidbody2D>().velocity = new Vector2(0, 1);
            }
        }

        if (transform.position.y < -4.5)
        {
            GameObject.Find("Canvas").GetComponent<DisplayController>().DisplayGameOver();
            if (Input.GetKeyDown(KeyCode.Z))
            {
                SceneManager.LoadScene(0);
            }
        }

    }

    void OnTriggerEnter2D(Collider2D other)
    {

        if (other.gameObject.CompareTag("Ladder"))
        {
            isLad = true;
        }


        if (other.gameObject.CompareTag("Goal"))
        {
            GameObject.Find("Canvas").GetComponent<DisplayController>().DisplayGoal();
            if (Input.GetKeyDown(KeyCode.Z))
            {

            }
        }
    }


    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Ladder"))
        {
            isLad = false;
        }
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.CompareTag("Stage"))
        {
            isStage = true;
        }
    }
}
